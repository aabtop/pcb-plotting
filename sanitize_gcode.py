#!/usr/bin/env python3
import re
import sys
from decimal import Decimal
from pathlib import Path
from typing import Optional


PEN_OFFSET = (-54, 20, 0.9)
ADJUST_OFFSET = (84, 62, 0)
MINS = (0, 0, 0.9)
MAXES = (160, 200, 50)

END_GCODE = """
G91 ; Set to relative positioning
G1 Z30 F1200 ; Lift
G90 ; Set to absolute again
G0 X0 Y180 ; Move to home and forward
M84 ; Disable steppers
M106 S0 ; Turn fan off
M82 ;absolute extrusion mode
""".lstrip(
    "\n"
)

STATE = "START"


def apply_offset(param: str) -> str:
    """
    Apply an offset from PEN_OFFSET to a G1 parameter, as necessary.
    """
    if param[0] not in "XYZ":
        return param

    axis, value = param[0], Decimal(param[1:])
    axis_index = "XYZ".index(axis)
    value += Decimal(PEN_OFFSET[axis_index])
    value += Decimal(ADJUST_OFFSET[axis_index])
    value = max(MINS[axis_index], min(MAXES[axis_index], value))
    return axis + str(value)


def process_line(line: str) -> Optional[str]:
    global STATE

    command, comment = re.search(
        r"^(?P<command>.*?)(?P<comment>\W*(?:\(.*\)|;.*))?$", line
    ).groups()

    if comment:
        stripped_comment = comment.strip("() ;")
        line = f"{command} ; {stripped_comment}\n".strip(" ")

    if re.search(r"^(M104|M109|M140|M190|M105|M106|M107)( .*)?$", command):
        # Blacklisted lines.
        return

    m = re.match(r";\s*LAYER:(\d+)$", line.strip("\n"))
    if m:
        # Stop after the first layer.
        if int(m.group(1)) != 0:
            STATE = "END"
            return END_GCODE
        else:
            return line

    if not command:
        # Preserve comments and blank lines as they are.
        return line

    if re.match(r"(G(28|91|90)|M(84|106|82))($|\W)", command):
        # Whitelist some commands.
        return line

    if re.match("G0?[01] ", command):
        # Remove extruder moves.
        coords = command.split(" ")[1:]
        # Get the first character of each parameters.
        axes = "".join(sorted([x[0] for x in coords]))
        if axes in ["E", "EF", "S"]:
            # If it's just an extruder move or some weird spindle crap, skip it.
            return
        else:
            new_coords = []
            for coord in coords:
                if coord[0] in "ES":
                    continue
                new_coords.append(apply_offset(coord))
            if not new_coords:
                return
            return "G1 " + " ".join(new_coords) + "\n"

    # Skip all lines we don't understand.
    return None


def main(infile, outdir=None):
    p = Path(infile)
    i = p.open()
    outfile = p.with_name("sanitized_" + p.stem + ".gcode")
    if outdir:
        outfile = Path(outdir) / outfile.name

    print(f"Saving to {outfile}...")
    o = outfile.open("w")
    for line in i:
        output = process_line(line)
        if output is not None:
            o.write(output)

        if STATE == "END":
            break
    i.close()
    o.close()


if __name__ == "__main__":
    print("Sanitizing...")
    if len(sys.argv) == 2:
        main(sys.argv[1])
    elif len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    else:
        sys.exit("Input file not specified.")
    print("Done.")
