PCB Plotting
============

This repository contains information about a way to easily fabricate PCBs at home that I've been working on.
It consists of various parts.
The general workflow is:

* Design your PCB wherever (KiCAD recommended).
* Export the Gerbers.
* Run pcb2gcode to convert the Gerbers to Gcode for a CNC mill.
* Run `sanitize_gcode.py` to make the Gcode suitable for running on a Marlin 3D printer.
* Mount a permanent marker onto your printer and run the file you generated in the previous step.
  This will draw the layer onto the PCB.
* Throw the PCB into a tub of etchant.
* As a bonus, if your 3D printer has a movable bed, put the etchant onto it and run `agitator.gcode`.
  You're welcome.
* Done.
